//
//  CharacterDetailVC.swift
//  Marvel
//
//  Created by Vinícius Brito on 17/05/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit

class CharacterDetailVC: UIViewController {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var hqButton: UIBarButtonItem!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var character = DataModel()
    var hq = [HQ]()
    let hqID = "HQDetailVC"
    var hqStoryboard = "Main"

    // MARK: - <Lifecycle>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDetails(details: character)
    }
    
    // MARK: - <IBActions>
    
    @IBAction func showHQ(_ sender: Any) {
        activityIndicator.startAnimating()
        let ts = NSDate().timeIntervalSince1970.description
        
        /*
         Make the API request in order to get HQs of a specific character.
         */
        Webservice.getHQDetails(characterId: character.id, offset: 0, timeStamp: ts, md5Hex: Utilities.hashing(timeStamp: ts)) { (success, json) in
            
            /*
            Handling API Errors as per requirement. Showing the error message on screen.
            */
            if let json = json {
                if json["message"].stringValue != "" {
                    let message = json["message"].stringValue
                    self.alert(message: message)
                    self.activityIndicator.stopAnimating()
                } else {
                    let hqModel = HQ.parseList(json: json)
                    self.hq = hqModel
                    self.hqDetails(hqDetails: self.hq)
                    self.activityIndicator.stopAnimating()
                }
            }
        }
        
    }
    
    // MARK: - <Methods>
    
    /*
     Load characters' data.
     */
    func loadDetails(details: DataModel) {
        self.title = details.name
        
        for item in details.thumbnail {
            let urlWithExtension = String(format: "%@.%@", item.path, item.fileExtension)
            photoImageView.contentMode = UIView.ContentMode.scaleAspectFit
            
            if urlWithExtension == Config.imageNotFound() {
                photoImageView.image = #imageLiteral(resourceName: "placeholder")
            } else {
                
                if let url = URL(string: urlWithExtension) {
                    photoImageView.kf.setImage(with: url)
                } else {
                    photoImageView.image = #imageLiteral(resourceName: "placeholder")
                }
            }
        }
        
        if details.desc == "" {
            descriptionTextView.text = Config.noDescription()
        } else {
            descriptionTextView.text = details.desc
        }
        
    }
    
    /*
     Animation on screen transtition as per requirement.
     */
    func animationOnTransition(viewController: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType(rawValue: "flip")
        transition.subtype = CATransitionSubtype.fromTop
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(viewController, animated: false)
    }

    /*
     Navigate to HQ Details screen.
     */
    func hqDetails(hqDetails: [HQ]) {
        
        if hqDetails.isEmpty {
            alert(message: "No HQ available")
        } else {
            if let hqDetailVC = UIStoryboard(name: self.hqStoryboard, bundle: nil).instantiateViewController(withIdentifier: self.hqID) as? HQDetailVC {
                hqDetailVC.hqDetails = hqDetails
                self.animationOnTransition(viewController: hqDetailVC)
            }
        }
        
    }
    
    func alert(message: String) {
        let alertController = UIAlertController(title: "Oops!",
                                                message: message,
                                                preferredStyle: .alert)
        let ok = UIAlertAction(title: "Got it!",
                               style: .default) { (UIAlertAction) in
                                self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: nil)
    }
}

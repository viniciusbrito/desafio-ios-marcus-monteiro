//
//  HQ.swift
//  Marvel
//
//  Created by Vinícius Brito on 20/05/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import Foundation
import SwiftyJSON

class HQ: NSObject {
    
    var title : String = ""
    var desc: String = ""
    var thumbnail: [Thumbnail] = []
    var prices: [Prices] = []
    
    // MARK: - Parse JSON list
    
       static func parseList(json: JSON) -> [HQ] {
           var list = [HQ]()
           
           for oneElement in json["data"]["results"].arrayValue {
               let hqModel = HQ.parse(json: oneElement)
               list.append(hqModel)
           }
           
           return list
       }
       
    // MARK: - Parse JSON
    
       static func parse(json: JSON) -> HQ {
           let hqModel = HQ()
           
           hqModel.title = json["title"].stringValue
           hqModel.desc = json["description"].stringValue
           hqModel.thumbnail = Thumbnail.parse(json: json["thumbnail"])
           hqModel.prices = Prices.parseList(json: json["prices"])
                   
           return hqModel
       }
}

// MARK: - Prices

class Prices: NSObject {
    
    var type: String = ""
    var price: Double = 0.0
    
    // MARK: - Parse Price list
    
    static func parseList(json: JSON) -> [Prices] {
        var priceList = [Prices]()
        
        for oneElement in json.arrayValue {
            let priceModel = Prices.parse(json: oneElement)
            priceList.append(contentsOf: priceModel)
        }
        
        return priceList
        
    }
    
    // MARK: - Parse Price JSON
    
    static func parse(json: JSON) -> [Prices] {
        let price = Prices()
        
        price.type = json["type"].stringValue
        price.price = json["price"].doubleValue
        
        return [price]
    }
}

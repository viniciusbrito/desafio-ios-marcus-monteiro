//
//  DataModel.swift
//  Marvel
//
//  Created by Vinícius Brito on 16/05/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit
import SwiftyJSON

class DataModel: NSObject {
    
    var id: Int = 0
    var name : String = ""
    var desc: String = ""
    var thumbnail: [Thumbnail] = []

    // MARK: - Parse JSON list
    
    static func parseList(json: JSON) -> [DataModel] {
        var list = [DataModel]()
        
        for oneElement in json["data"]["results"].arrayValue {
            let dataModel = DataModel.parse(json: oneElement)
            list.append(dataModel)
        }
        
        return list
    }
    
    // MARK: - Parse JSON
    
    static func parse(json: JSON) -> DataModel {
        let dataModel = DataModel()
        
        dataModel.id = json["id"].intValue
        dataModel.name = json["name"].stringValue
        dataModel.desc = json["description"].stringValue
        dataModel.thumbnail = Thumbnail.parse(json: json["thumbnail"])
                
        return dataModel
    }
    
}

// MARK: - Thumbnail

class Thumbnail: NSObject {
    var path: String = ""
    var fileExtension: String = ""
    
    // MARK: - Parse Thumbnail
    
    static func parse(json: JSON) -> [Thumbnail] {
        
        let thumbnail = Thumbnail()
        thumbnail.path = json["path"].stringValue
        thumbnail.fileExtension = json["extension"].stringValue
        
        return [thumbnail]
    }
}

//
//  CharactersVC.swift
//  Marvel
//
//  Created by Vinícius Brito on 16/05/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CharactersVC: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var charactersTableView: UITableView!
    @IBOutlet weak var refreshBarButton: UIBarButtonItem!
    
    var charactersList = [DataModel]()
    var actualPage: Int = 0
    var totalCharacters: Int = 0
    var isAllDataLoaded = false
    var isNewDataLoading = false
    var cellIdentifier = "CharacterCell"
    var characterStoryboard = "Main"
    var detailVCIdentifier = "CharacterDetailVC"
    
    // MARK: - <Lifecycle>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        charactersTableView.isHidden = true
        customNavigationBar()
        setTableView(tableView: charactersTableView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.barStyle = .black
    }
    
    // MARK: - <CustomUI>
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func customNavigationBar() {
        let imageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 44))
        imageView.frame = titleView.bounds
        titleView.addSubview(imageView)
        self.navigationItem.titleView = titleView
    }
    
    // MARK: - <IBActions>
    
    @IBAction func refresh(_ sender: Any) {
        refreshBarButton.isEnabled = false
        charactersTableView.isHidden = true
        updateCharactersList(page: actualPage)
    }
    
    // MARK: - <Methods>
    
    /*
     Register the cell and call the request method in order to get characters list.
     */
    func setTableView(tableView: UITableView) {
        tableView.register(UINib.init(nibName: "CharacterCell", bundle: nil),
        forCellReuseIdentifier: cellIdentifier)
    }
    
    /*
     Method to get the list of Marvel's characters.
     */
    func updateCharactersList(page: Int) {
        activityIndicator.startAnimating()
        guard isAllDataLoaded == false else {
            return
        }
        
        isNewDataLoading = true
        
        let ts = NSDate().timeIntervalSince1970.description
        Webservice.getCharacters(offset: actualPage, timeStamp: ts, md5Hex: Utilities.hashing(timeStamp: ts)) { (success, json) in
            /*
             Handling API Errors as per requirement. Showing the error message on screen.
             */
            if let json = json {
                if json["message"].stringValue != "" {
                    let message = json["message"].stringValue
                    self.alert(message: message)
                    self.activityIndicator.stopAnimating()
                } else {
                    self.totalCharacters = json["data"]["total"].intValue
                        
                        let dataModel = DataModel.parseList(json: json)
                        
                        if dataModel.count == 0 {
                            self.isAllDataLoaded = true
                            if self.actualPage == 0 {
                                self.charactersList = []
                            }
                        } else {
                            self.actualPage += 20
                            if self.actualPage <= 20 {
                                self.charactersList = dataModel
                            } else {
                                self.charactersList += dataModel
                            }
                            
                            if self.charactersList.count >= self.totalCharacters {
                                self.isAllDataLoaded = true
                            }
                        }
                        
                        self.charactersTableView.reloadData()
                        self.isNewDataLoading = false
                        self.charactersTableView.isHidden = false
                        self.refreshBarButton.isEnabled = true
                        self.activityIndicator.stopAnimating()
                    }
                
                }
                
        }
    }
    
    /*
     Animation on screen transtition as per requirement.
     */
    func animationOnTransition(viewController: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType(rawValue: "flip")
        transition.subtype = CATransitionSubtype.fromLeft
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    func alert(message: String) {
        let alertController = UIAlertController(title: "Oops!",
                                                message: message,
                                                preferredStyle: .alert)
        let ok = UIAlertAction(title: "Got it!",
                               style: .default) { (UIAlertAction) in
                                self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

// MARK: - <UITableView>

extension CharactersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return charactersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CharacterCell {
            let character = self.charactersList[indexPath.row]
            cell.setup(dataModel: character)
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        charactersTableView.deselectRow(at: indexPath, animated: true)
        let character = self.charactersList[indexPath.row]
        
        if let characterDetailVC = UIStoryboard(name: characterStoryboard, bundle: nil).instantiateViewController(withIdentifier: detailVCIdentifier) as? CharacterDetailVC {
            characterDetailVC.character = character
            animationOnTransition(viewController: characterDetailVC)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == charactersTableView {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= (scrollView.contentSize.height/3)*2.5) {
                if !isNewDataLoading {
                    isNewDataLoading = true
                    updateCharactersList(page: actualPage)
                }
            }
        }
    }
    
}

//
//  HQDetailVC.swift
//  Marvel
//
//  Created by Vinícius Brito on 20/05/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit

class HQDetailVC: UIViewController {
        
    @IBOutlet weak var hqTableView: UITableView!
    
    var hqDetails = [HQ]()
    var prices = [Prices]()
    var price = [Double]()
    //var maxPrice = Double()
    var hqTitle = [String]()
    var hqDescription = [String]()
    var hqThumbnail = [Thumbnail]()
    var hqPrice = [Double]()
    let highestPrice = "printPrice"
    let cellIdentifier = "HQCell"

    // MARK: - <Lifecycle>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMaxPrice(hqDetails: hqDetails)
    }
    
    // MARK: - <Methods>
    
    /*
     Register TableView's cell.
     */
    func setTableView(tableView: UITableView) {
        tableView.register(UINib.init(nibName: "HQCell", bundle: nil),
        forCellReuseIdentifier: cellIdentifier)
    }
    
    /*
     Get most expensive HQ(s) price of a specific character.
     */
    func getMaxPrice(hqDetails: [HQ]) {
        self.title = "Most Expensive HQ(s)"
        for item in hqDetails {
            prices.append(contentsOf: item.prices)
        }
        
        for value in prices {
            if value.type == highestPrice {
                price.append(value.price)
            }
        }
        
        if let mxPrice = price.max() {
            loadDataForMaxPrice(maxPrice: mxPrice)
        } else {
            print("No HQ available")
        }
        
    }

    /*
     Load data in order to populate TableView with HQ details.
     */
    func loadDataForMaxPrice(maxPrice: Double) {
        for item in hqDetails {
            for anotherItem in item.prices {
                if anotherItem.price == maxPrice {
                    hqTitle.append(item.title)
                    hqDescription.append(item.desc)
                    hqThumbnail.append(contentsOf: item.thumbnail)
                    hqPrice.append(anotherItem.price)
                }
            }
        }
        setTableView(tableView: hqTableView)
    }
    
}

// MARK: - <UITableView>

extension HQDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hqTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? HQCell {
            let title = self.hqTitle[indexPath.row]
            let description = self.hqDescription[indexPath.row]
            let thumbnail = self.hqThumbnail[indexPath.row]
            let price = self.hqPrice[indexPath.row]
            cell.setup(title: title, description: description, thumbnail: thumbnail, price: price)
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
}

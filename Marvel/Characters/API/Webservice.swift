//
//  Webservice.swift
//  Marvel
//
//  Created by Vinícius Brito on 15/05/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct Config {
    
    // MARK: - Endpoints
    
    static func baseUrl() -> String {
        let url = "https://gateway.marvel.com"
        return url
    }
    
    static func charactersEndpoint() -> String {
        let charactersEndpoint = "/v1/public/characters"
        return charactersEndpoint
    }
    
    // MARK: - Properties
    
    static func apiKey() -> String {
        let apiKey = "8d6ceaf78b59f57596f07271d6d99177"
        return apiKey
    }
    
    static func prvtKey() -> String {
        let prvtKey = "325944a049c92bf03f7c46a80efb03f040f8ac6b"
        return prvtKey
    }
    
    static func imageNotFound() -> String {
        let imageNotFound = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
        return imageNotFound
    }
    
    static func noDescription() -> String {
        let description = "No description available."
        return description
    }
    
    // MARK: - API Errors
    
    static func timeOut() -> String {
        let timeOutMessage = "The request timed out."
        return timeOutMessage
    }
}

class Webservice {
    
    /*
     Get the list of Marvel's characters
     */
    class func getCharacters(offset: Int, timeStamp: String, md5Hex: String, completion: @escaping (Bool, JSON?) -> ()) {
                    
        let url = "\(Config.baseUrl())\(Config.charactersEndpoint())"
        
        let parameters : Parameters = [
            "apikey": Config.apiKey(),
            "ts": timeStamp,
            "hash": md5Hex,
            "offset": offset]
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON {
            (response) in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    /*
                     Handling API Errors as per requirement.
                     */
                    switch response.response?.statusCode {
                    case 401:
                        print("Invalid Referer or Invalid Hash")
                    case 403:
                        print("Forbidden")
                    case 405:
                        print("Method Not Allowed")
                    case 409:
                        print("Missing API Key or Missing Hash or Missing Timestamp")
                    default:
                        break
                    }
                    
                    let json = JSON(value)
                    completion(json.dictionary != nil, json)
                }
            case .failure(let error):
                if error.localizedDescription != Config.timeOut() {
                    print("Error: \(error.localizedDescription)")
                } else {
                    print("Error: \(Config.timeOut())")
                }
            }
        }
        
    }
    
    
    /*
     Get available HQs of a specific character
     */
    class func getHQDetails (characterId: Int, offset: Int, timeStamp: String, md5Hex: String,completion: @escaping (Bool, JSON?) -> ()) {
        
        let hqEndpoint = "/v1/public/characters/\(characterId)/comics"
        let url =  "\(Config.baseUrl())\(hqEndpoint)"
        
        let parameters : Parameters = [
        "apikey": Config.apiKey(),
        "ts": timeStamp,
        "hash": md5Hex,
        "offset": offset]
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON {
            (response) in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    /*
                     Handling API Errors as per requirement.
                     */
                    switch response.response?.statusCode {
                    case 409:
                        print("Limit greater than 100 / Limit invalid or below 1 / Invalid or unrecognized parameter / Empty parameter / Invalid or unrecognized ordering parameter / Too many values sent to a multi-value list filter / Invalid value passed to filter")
                    default:
                        break
                    }
                    
                    let json = JSON(value)
                    completion(json.dictionary != nil, json)
                }
            case .failure(let error):
                if error.localizedDescription != Config.timeOut() {
                    print("Error: \(error.localizedDescription)")
                } else {
                    print("Error: \(Config.timeOut())")
                }
            }
        }
        
    }
    
}




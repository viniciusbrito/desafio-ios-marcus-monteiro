//
//  CharacterCell.swift
//  Marvel
//
//  Created by Vinícius Brito on 16/05/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit
import Kingfisher

class CharacterCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    /*
     Load data for the cell.
     */
    func setup(dataModel: DataModel) {
        activityIndicator.startAnimating()
        
        for item in dataModel.thumbnail {
            let urlWithExtension = String(format: "%@.%@", item.path, item.fileExtension)
            photo.contentMode = UIView.ContentMode.scaleAspectFit
            
            if urlWithExtension == Config.imageNotFound() {
                photo.image = #imageLiteral(resourceName: "placeholder")
                activityIndicator.stopAnimating()
            } else {
                
                if let url = URL(string: urlWithExtension) {
                    photo.kf.setImage(with: url)
                } else {
                    photo.image = #imageLiteral(resourceName: "placeholder")
                }
                activityIndicator.stopAnimating()
            }
        }
        
        name.text = dataModel.name
    }
}

//
//  HQCell.swift
//  Marvel
//
//  Created by Vinícius Brito on 20/05/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit

class HQCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var hqImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /*
     Load data for the cell.
     */
    func setup(title: String, description: String, thumbnail: Thumbnail, price: Double) {
        

        self.bringSubviewToFront(activityIndicator)



        
        
        activityIndicator.startAnimating()
        titleLabel.text = title
        
        let urlWithExtension = String(format: "%@.%@", thumbnail.path, thumbnail.fileExtension)
        hqImageView.contentMode = UIView.ContentMode.scaleAspectFit
            
        if urlWithExtension == Config.imageNotFound() {
            hqImageView.image = #imageLiteral(resourceName: "placeholder")
            activityIndicator.stopAnimating()
        } else {
                
            if let url = URL(string: urlWithExtension) {
                hqImageView.kf.setImage(with: url)
            } else {
                hqImageView.image = #imageLiteral(resourceName: "placeholder")
            }
            activityIndicator.stopAnimating()
        }
                
        if description == "" {
            descriptionTextView.text = Config.noDescription()
        } else {
            descriptionTextView.text = description
        }
        
        priceLabel.text = String(format: "Price: US$ %.2f", price)
    }
    
}

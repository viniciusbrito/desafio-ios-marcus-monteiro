//
//  Utilities.swift
//  Marvel
//
//  Created by Vinícius Brito on 18/05/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import Foundation
import CryptoKit

class Utilities {
    
    /*
     Function that returns MD5 hash.
     */
    class func MD5(string: String) -> String {
        let digest = Insecure.MD5.hash(data: string.data(using: .utf8) ?? Data())

        return digest.map {
            String(format: "%02hhx", $0)
        }.joined()
    }
        
    /*
     Hashing properties for API requests as per API requirements.
     */
    class func hashing(timeStamp: String) -> String {
        let stringForHashing = String(format: "%@%@%@", timeStamp, Config.prvtKey(), Config.apiKey())
        let md5Data = MD5(string:stringForHashing)
        
        return md5Data
    }
    
}
